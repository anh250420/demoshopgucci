//
//  TableViewCell.swift
//  ShopGUCCI
//
//  Created by devsenior1 on 28/04/2022.
//

import UIKit
protocol SendBackDataProtocol: class {
    func sendBackDataFunc(data: Int?)
}
class TableViewCell: UITableViewCell {
    var delegate: SendBackDataProtocol?
    var a: Int = 1
    @IBOutlet weak var USUK: UILabel!
    @IBOutlet weak var SizeG: UILabel!
    @IBOutlet weak var YeuThich: UIButton!
    @IBOutlet weak var LBSoLuong: UILabel!
    @IBOutlet weak var BTNTru: UIButton!
    @IBOutlet weak var BTNCong: UIButton!
    @IBOutlet weak var ImageTableView: UIImageView!
    @IBOutlet weak var NameTBV: UILabel!
    @IBOutlet weak var Giatri: UILabel!
    var imageName:String = "yeu.png"
    override func awakeFromNib() {
        super.awakeFromNib()
        BTNTru.layer.cornerRadius = 10
        BTNCong.layer.cornerRadius = 10
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func Tru(_ sender: Any) {
        if a > 0 {
            a -= 1
            LBSoLuong.text = "\(a)"
            self.delegate?.sendBackDataFunc(data: a)
        }
    }
    
    @IBAction func Cong(_ sender: Any) {
        
        a += 1
        LBSoLuong.text = "\(a)"
        self.delegate?.sendBackDataFunc(data: a)
    }
    @IBAction func onoff(_ sender: Any) {
        
        if imageName == "yeu.png"
        {
            imageName = "KYeu.png"
        }
        else{
            imageName = "yeu.png"
        }
        YeuThich.setImage(UIImage(named: imageName), for: .normal)
    }
    
}


