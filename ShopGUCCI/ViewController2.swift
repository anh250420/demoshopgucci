//
//  ViewController2.swift
//  ShopGUCCI
//
//  Created by devsenior1 on 27/04/2022.
//

import UIKit
import AVFoundation

class ViewController2: UIViewController, UITableViewDelegate, UITableViewDataSource, SendBackDataProtocol {
    
    var b: Int = 0
    var c: Int = 0
    var d: Int = 0
    @IBOutlet weak var TongGiaTri: UILabel!
    @IBOutlet weak var sob: UILabel!
    @IBOutlet weak var TableView: UITableView!
    @IBOutlet weak var CheckOutBtn: UIButton!
    @IBOutlet weak var Back: UIButton!
    private var recipes = Recipe.createRecipes()
    var listDB:[Recipe] = []
    private let cellIdentifier: String = "TableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        TableView.dataSource = self
        TableView.delegate = self
        
        let mygiatri = NSMutableAttributedString(string: "$ \(c)")
        TongGiaTri.attributedText = mygiatri
        
        CheckOutBtn.layer.cornerRadius = 31
        // Do any additional setup after loading the view.
    }
    func sendBackDataFunc(data: Int?) {
        d = data!
        TongGiaTri.text = "$ \(c * d).00"
        }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        b = listDB.count
        let myString = NSMutableAttributedString(string: "\(b)")
        sob.attributedText = myString
        return listDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? TableViewCell {
            cell.delegate = self
            cell.NameTBV.text = "\(listDB[indexPath.row].name)"
            cell.Giatri.text = "$ \(listDB[indexPath.row].prepTime)"
            cell.imageView!.image = UIImage(named: "\(listDB[indexPath.row].thumbnails)")
            cell.SizeG.text = "\(listDB[indexPath.row].size)"
            cell.USUK.text = "\(listDB[indexPath.row].NguonSize)"
            c = listDB[indexPath.row].prepTime
            TongGiaTri.text = "$ \(c).00"
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            listDB.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .bottom)
            if b > 0 {
                b = b - 1
            }
        }
    }
    
    @IBAction func BackM1(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
  
}
extension ViewController2 {

    private func setupUI() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: nil)
        navigationItem.title = "Recipes"
        TableView.reloadData()
    }

}

