//
//  ImageViewController.swift
//  ShopGUCCI
//
//  Created by devsenior1 on 28/04/2022.
//

import UIKit

class ImageViewController: UIViewController {


    @IBOutlet weak var logoImageView: UIImageView!
    private var logoImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logoImageView.image = logoImage
       
    }
    func setupElements(image: UIImage) {
        logoImage = image
    }

}
