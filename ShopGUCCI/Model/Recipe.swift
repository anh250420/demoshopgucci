//
//  Recipe.swift
//  TableView
//
//  Created by BILAL ARSLAN on 25.12.2018.
//  Copyright © 2018 Bilal ARSLAN. All rights reserved.
//
import UIKit
import Foundation

struct Recipe {
    let name: String
    let thumbnails: String
    let prepTime: Int
    var size: Double
    var NguonSize: String
}

extension Recipe {
    static func createRecipes() -> [Recipe] {
        return [Recipe(name: "Nike Air Max", thumbnails: "GUCCI1.png", prepTime: Int(290.00), size: 0, NguonSize: "Uk"),
                Recipe(name: "Nike Wmns", thumbnails: "GUCCI3.png", prepTime: Int(200.00), size: 0, NguonSize: "US"),
                Recipe(name: "Nike Flyknit", thumbnails: "GUCCI4.png", prepTime: Int(250.00), size: 0, NguonSize: "EU")]
    }
}

