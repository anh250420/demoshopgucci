//
//  TrangchuViewController.swift
//  ShopGUCCI
//
//  Created by devsenior1 on 04/05/2022.
//

import UIKit

class TrangchuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    public var listdata = Recipe.createRecipes()
    
    @IBOutlet weak var TrangChuTBView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TrangChuTBView.register(UINib(nibName: "TrangchuTableViewCell", bundle: nil), forCellReuseIdentifier: "TrangchuTableViewCell")
        TrangChuTBView.dataSource = self
        TrangChuTBView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.listdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"TrangchuTableViewCell", for: indexPath) as! TrangchuTableViewCell
        cell.LBTT.text = self.listdata[indexPath.row].name
        cell.imgTT.image = UIImage(named: self.listdata[indexPath.row].thumbnails)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        sb.itemSend = self.listdata[indexPath.row]
        self.navigationController?.pushViewController(sb, animated: true)

    }

}
