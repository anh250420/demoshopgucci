//
//  ViewController.swift
//  ShopGUCCI
//
//  Created by devsenior1 on 27/04/2022.
//

import UIKit
import DLAutoSlidePageViewController

class ViewController: UIViewController, CollapsibleTableViewHeaderDelegate{
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int) {
        let collapsed = !sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        
        HangMucTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    var listdatabase: [Recipe] = []
    var size:Double = 0.0
    var NSize:String = "UK"
    public var recipes = Recipe.createRecipes()
    var itemSend:Recipe = Recipe(name: "", thumbnails: "", prepTime: 0, size: 0, NguonSize: "")
    var a: Int = 0
    @IBOutlet weak var GiaTien: UILabel!
    @IBOutlet weak var nameGiay: UILabel!
    @IBOutlet weak var Soluong: UILabel!
    @IBOutlet weak var HangMucTableView: UITableView!
    var sections = sectionsData
    @IBOutlet weak var BtnSize7: UIButton!
    @IBOutlet weak var BtnSize6: UIButton!
    @IBOutlet weak var BtnSize5: UIButton!
    @IBOutlet weak var BtnSize4: UIButton!
    @IBOutlet weak var BtnSize3: UIButton!
    @IBOutlet weak var BtnSize2: UIButton!
    @IBOutlet weak var BtnSize1: UIButton!
    @IBOutlet weak var USBtn: UIButton!
    @IBOutlet weak var UKBtn: UIButton!
    @IBOutlet weak var EU: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var NextBtn: UIButton!
    @IBOutlet weak var AddCartBtn: UIButton!
    
    let ImageViewControllerIdentifier = "ImageViewController"
    override func viewDidLoad() {
        super.viewDidLoad()
        nameGiay.text = itemSend.name
        GiaTien.text = "$ \(itemSend.prepTime).00"
        HangMucTableView.dataSource = self
        HangMucTableView.delegate = self
        setupElements()
        BtnSize1.layer.cornerRadius = 10
        BtnSize2.layer.cornerRadius = 10
        BtnSize3.layer.cornerRadius = 10
        BtnSize4.layer.cornerRadius = 10
        BtnSize5.layer.cornerRadius = 10
        BtnSize6.layer.cornerRadius = 10
        BtnSize7.layer.cornerRadius = 10
        AddCartBtn.layer.cornerRadius = 31
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 50
        containerView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        // Do any additional setup after loading the view.
    }
    private func setupElements(){
        setupPageViewController()
    }
    private func setupPageViewController() {
        let pages: [UIViewController] = setupPages()

        // Configure default auto slide page configuration
//        DefaultAutoSlideConfiguration.shared.timeInterval = 2.0
        let pageViewController = DLAutoSlidePageViewController(pages: pages)
        
        addChild(pageViewController)
        containerView.addSubview(pageViewController.view)
        pageViewController.view.frame = containerView.bounds
    }

    private func setupPages() -> [UIViewController] {
        let Sub1 = storyboard?.instantiateViewController(withIdentifier: ImageViewControllerIdentifier) as! ImageViewController
        Sub1.setupElements(image: #imageLiteral(resourceName: "GUCCI 1"))

        let Sub2 = storyboard?.instantiateViewController(withIdentifier: ImageViewControllerIdentifier) as! ImageViewController
        Sub2.setupElements(image: #imageLiteral(resourceName: "Gucci 4"))

        let Sub3 = storyboard?.instantiateViewController(withIdentifier: ImageViewControllerIdentifier) as! ImageViewController
        Sub3.setupElements(image: #imageLiteral(resourceName: "Gucci 3"))
        
        let Sub4 = storyboard?.instantiateViewController(withIdentifier: ImageViewControllerIdentifier) as! ImageViewController
        Sub4.setupElements(image: #imageLiteral(resourceName: "GUCCI 2"))

        return [Sub1, Sub2, Sub3, Sub4]
    }

    
    @IBAction func SizeBtn7(_ sender: Any) {
        size = 8
        BtnSize1.backgroundColor = UIColor.systemGray5
        BtnSize1.setTitleColor(UIColor.black, for: .normal)
        BtnSize2.backgroundColor = UIColor.systemGray5
        BtnSize2.setTitleColor(UIColor.black, for: .normal)
        BtnSize3.backgroundColor = UIColor.systemGray5
        BtnSize3.setTitleColor(UIColor.black, for: .normal)
        BtnSize4.backgroundColor = UIColor.systemGray5
        BtnSize4.setTitleColor(UIColor.black, for: .normal)
        BtnSize5.backgroundColor = UIColor.systemGray5
        BtnSize5.setTitleColor(UIColor.black, for: .normal)
        BtnSize6.backgroundColor = UIColor.systemGray5
        BtnSize6.setTitleColor(UIColor.black, for: .normal)
        BtnSize7.backgroundColor = UIColor.orange
        BtnSize7.setTitleColor(UIColor.white, for: .normal)
    }
    @IBAction func SizeBtn6(_ sender: Any) {
        size = 7.5
        BtnSize1.backgroundColor = UIColor.systemGray5
        BtnSize1.setTitleColor(UIColor.black, for: .normal)
        BtnSize2.backgroundColor = UIColor.systemGray5
        BtnSize2.setTitleColor(UIColor.black, for: .normal)
        BtnSize3.backgroundColor = UIColor.systemGray5
        BtnSize3.setTitleColor(UIColor.black, for: .normal)
        BtnSize4.backgroundColor = UIColor.systemGray5
        BtnSize4.setTitleColor(UIColor.black, for: .normal)
        BtnSize5.backgroundColor = UIColor.systemGray5
        BtnSize5.setTitleColor(UIColor.black, for: .normal)
        BtnSize6.backgroundColor = UIColor.orange
        BtnSize6.setTitleColor(UIColor.white, for: .normal)
        BtnSize7.backgroundColor = UIColor.systemGray5
        BtnSize7.setTitleColor(UIColor.black, for: .normal)
    }
    @IBAction func SizeBtn5(_ sender: Any) {
        size = 7
        BtnSize1.backgroundColor = UIColor.systemGray5
        BtnSize1.setTitleColor(UIColor.black, for: .normal)
        BtnSize2.backgroundColor = UIColor.systemGray5
        BtnSize2.setTitleColor(UIColor.black, for: .normal)
        BtnSize3.backgroundColor = UIColor.systemGray5
        BtnSize3.setTitleColor(UIColor.black, for: .normal)
        BtnSize4.backgroundColor = UIColor.systemGray5
        BtnSize4.setTitleColor(UIColor.black, for: .normal)
        BtnSize5.backgroundColor = UIColor.orange
        BtnSize5.setTitleColor(UIColor.white, for: .normal)
        BtnSize6.backgroundColor = UIColor.systemGray5
        BtnSize6.setTitleColor(UIColor.black, for: .normal)
        BtnSize7.backgroundColor = UIColor.systemGray5
        BtnSize7.setTitleColor(UIColor.black, for: .normal)
    }
    @IBAction func SizeBtn4(_ sender: Any) {
        size = 6.5
        BtnSize1.backgroundColor = UIColor.systemGray5
        BtnSize1.setTitleColor(UIColor.black, for: .normal)
        BtnSize2.backgroundColor = UIColor.systemGray5
        BtnSize2.setTitleColor(UIColor.black, for: .normal)
        BtnSize3.backgroundColor = UIColor.systemGray5
        BtnSize3.setTitleColor(UIColor.black, for: .normal)
        BtnSize4.backgroundColor = UIColor.orange
        BtnSize4.setTitleColor(UIColor.white, for: .normal)
        BtnSize5.backgroundColor = UIColor.systemGray5
        BtnSize5.setTitleColor(UIColor.black, for: .normal)
        BtnSize6.backgroundColor = UIColor.systemGray5
        BtnSize6.setTitleColor(UIColor.black, for: .normal)
        BtnSize7.backgroundColor = UIColor.systemGray5
        BtnSize7.setTitleColor(UIColor.black, for: .normal)
    }
    @IBAction func SizeBtn3(_ sender: Any) {
        size = 6
        BtnSize1.backgroundColor = UIColor.systemGray5
        BtnSize1.setTitleColor(UIColor.black, for: .normal)
        BtnSize2.backgroundColor = UIColor.systemGray5
        BtnSize2.setTitleColor(UIColor.black, for: .normal)
        BtnSize3.backgroundColor = UIColor.orange
        BtnSize3.setTitleColor(UIColor.white, for: .normal)
        BtnSize4.backgroundColor = UIColor.systemGray5
        BtnSize4.setTitleColor(UIColor.black, for: .normal)
        BtnSize5.backgroundColor = UIColor.systemGray5
        BtnSize5.setTitleColor(UIColor.black, for: .normal)
        BtnSize6.backgroundColor = UIColor.systemGray5
        BtnSize6.setTitleColor(UIColor.black, for: .normal)
        BtnSize7.backgroundColor = UIColor.systemGray5
        BtnSize7.setTitleColor(UIColor.black, for: .normal)
    }
    @IBAction func SizeBtn2(_ sender: Any) {
        size = 5.5
        BtnSize1.backgroundColor = UIColor.systemGray5
        BtnSize1.setTitleColor(UIColor.black, for: .normal)
        BtnSize2.backgroundColor = UIColor.orange
        BtnSize2.setTitleColor(UIColor.white, for: .normal)
        BtnSize3.backgroundColor = UIColor.systemGray5
        BtnSize3.setTitleColor(UIColor.black, for: .normal)
        BtnSize4.backgroundColor = UIColor.systemGray5
        BtnSize4.setTitleColor(UIColor.black, for: .normal)
        BtnSize5.backgroundColor = UIColor.systemGray5
        BtnSize5.setTitleColor(UIColor.black, for: .normal)
        BtnSize6.backgroundColor = UIColor.systemGray5
        BtnSize6.setTitleColor(UIColor.black, for: .normal)
        BtnSize7.backgroundColor = UIColor.systemGray5
        BtnSize7.setTitleColor(UIColor.black, for: .normal)
    }
    @IBAction func SizeBtn1(_ sender: Any) {
        size = 5
        BtnSize1.backgroundColor = UIColor.orange
        BtnSize1.setTitleColor(UIColor.white, for: .normal)
        BtnSize2.backgroundColor = UIColor.systemGray5
        BtnSize2.setTitleColor(UIColor.black, for: .normal)
        BtnSize3.backgroundColor = UIColor.systemGray5
        BtnSize3.setTitleColor(UIColor.black, for: .normal)
        BtnSize4.backgroundColor = UIColor.systemGray5
        BtnSize4.setTitleColor(UIColor.black, for: .normal)
        BtnSize5.backgroundColor = UIColor.systemGray5
        BtnSize5.setTitleColor(UIColor.black, for: .normal)
        BtnSize6.backgroundColor = UIColor.systemGray5
        BtnSize6.setTitleColor(UIColor.black, for: .normal)
        BtnSize7.backgroundColor = UIColor.systemGray5
        BtnSize7.setTitleColor(UIColor.black, for: .normal)
    }
    @IBAction func BtnEU(_ sender: Any) {
        NSize = "EU"
        UKBtn.setImage(UIImage(named: "UK.png"), for: .normal)
        USBtn.setImage(UIImage(named: "US.png"), for: .normal)
        EU.setImage(UIImage(named: "EUD.png"), for: .normal)
    }
    @IBAction func BtnUS(_ sender: Any) {
        NSize = "US"
        UKBtn.setImage(UIImage(named: "UK.png"), for: .normal)
        USBtn.setImage(UIImage(named: "USD.png"), for: .normal)
        EU.setImage(UIImage(named: "EU.png"), for: .normal)
    }
    @IBAction func BtnUK(_ sender: Any) {
        NSize = "UK"
        UKBtn.setImage(UIImage(named: "UKD.png"), for: .normal)
        USBtn.setImage(UIImage(named: "US.png"), for: .normal)
        EU.setImage(UIImage(named: "EU.png"), for: .normal)
    }
    @IBAction func AddtoCart(_ sender: Any) {
        listdatabase.append(Recipe(name: itemSend.name, thumbnails:itemSend.thumbnails, prepTime: itemSend.prepTime, size:size, NguonSize: NSize))
        
       }
    @IBAction func NextGioHang(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
        vc.listDB = listdatabase
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func quayve(_ sender: Any) {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TrangchuViewController") as! TrangchuViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension ViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].collapsed ? 0 : sections[section].items.count
    }
    
    // Cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CollapsibleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CollapsibleTableViewCell ??
            CollapsibleTableViewCell(style: .default, reuseIdentifier: "cell")
        
        let item: Item = sections[indexPath.section].items[indexPath.row]
        
        cell.nameLabel.text = item.name
        cell.detailLabel.text = item.detail
        cell.imageAnh.image = UIImage(named: item.imagename)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    // Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
        
        header.titleLabel.text = sections[section].name
        header.arrowLabel.text = "^"
        header.setCollapsed(sections[section].collapsed)
        
        header.section = section
        header.delegate = self
        
        return header
    }
    

}

